import Foundation

enum MyOptional<Int> {
    case value(Int)
    case empty
}

func +(a: MyOptional<Int>, b: MyOptional<Int>) -> MyOptional<Int> {
    switch (a, b) {
    case (MyOptional.value(let int1), MyOptional.value(let int2)):
        return MyOptional.value(int1 + int2)
    case(MyOptional.value(let int), MyOptional.empty):
        return MyOptional.value(int)
    case(MyOptional.empty, MyOptional.value(let int)):
        return(MyOptional.value(int))
    default:
        return MyOptional.empty
    }
}

let a = MyOptional<Int>.empty
let b = MyOptional<Int>.value(10)
print(a + b)
