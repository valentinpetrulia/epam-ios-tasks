import Foundation

let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for element in array {
    print(element)
}

var i = 0
while i < array.count {
    print(array[i])
    i += 1
}

i = 0
repeat {
    print(array[i])
    i += 1
} while i < array.count 
