import Foundation

let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

// even numbers
print("Even numbers:")
print("\n\tFor-in-loop")
for element in array {
    if element % 2 == 0 {
        print("\t\t\(element)")
    }
}

print("\n\tWhile-loop")
var i = 0
while i < array.count {
    if array[i] % 2 == 0 {
        print("\t\t\(array[i])")
    }
    i += 1
}

print("\n\tRepeat-while-loop")
i = 0
repeat {
    if array[i] % 2 == 0 {
        print("\t\t\(array[i])")
    }
    i += 1
} while i < array.count


// odd numbers
print("\n\nOdd numbers:")
print("\n\tFor-in-loop")
for element in array {
    if element % 2 == 1 {
        print("\t\t\(element)")
    }
}

print("\n\tWhile-loop")
i = 0
while i < array.count {
    if array[i] % 2 == 1 {
        print("\t\t\(array[i])")
    }
    i += 1
}

print("\n\tRepeat-while-loop")
i = 0
repeat {
    if array[i] % 2 == 1 {
        print("\t\t\(array[i])")
    }
    i += 1
} while i < array.count
