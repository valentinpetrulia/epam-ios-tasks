import Foundation

enum Transmission {
    case variator
    case manumatic
    case automatic
    case manual
}


struct Car {
    var model: String
    var power: Int
    var transmission: Transmission
}

extension Car: CustomStringConvertible {
    var description: String {
        return "\(model)"
    }
}

func group(_ cars: [Car]) -> [[Car]] {
    let dict = Dictionary(grouping: cars, by: \.transmission)
    return Array(dict.values)
}

let car1 = Car(model: "car1", power: 1, transmission: .variator)
let car2 = Car(model: "car2", power: 1, transmission: .automatic)
let car3 = Car(model: "car3", power: 1, transmission: .manual)
let car4 = Car(model: "car4", power: 1, transmission: .manumatic)
let car5 = Car(model: "car5", power: 1, transmission: .variator)
let car6 = Car(model: "car6", power: 1, transmission: .automatic)
let cars = [car1, car2, car3, car4, car5, car6]

print(group(cars))


//extension Transmission: Hashable {}


extension Array {
    func group(by key: (Element) -> AnyHashable) -> [[Element]] {
        var dict = [AnyHashable: [Element]]()
        for element in self {
            let key = key(element)
            if dict.keys.contains(key) {
                dict[key]?.append(element)
            } else {
                dict[key] = [element]
            }
        }
        let array = dict.map { Array($0.value) }
        return array
    }
}

print(cars.group { $0.transmission })
