import Foundation

struct WeatherForecast {
    var date: Date
    var temperature: Double
}

enum WeatherError: Error {
    case error(message: String?)
}


func chance(ints: ClosedRange<UInt>) -> Bool {
    let random = UInt.random(in: 1...10)
    return ints.contains(random)
}

class WeatherDataProvider {
    static let shared = WeatherDataProvider()
    
    private var url: URL?
    
    private init() {}
    
    func configure(weatherServiceURL: URL)  {
        url = weatherServiceURL
    }
    
    func getWeatherThrows(date: Date) throws -> WeatherForecast {
        precondition(url != nil)
        if chance(ints: 1...7) {
            let weatherForecast = WeatherForecast(date: date, temperature: 20)
            return weatherForecast
        } else {
            throw WeatherError.error(message: "Bad chance")
        }
    }
    
    func getWeatherOptional(date: Date) -> WeatherForecast? {
        precondition(url != nil)
        if chance(ints: 1...7) {
            let weatherForecast = WeatherForecast(date: date, temperature: 20)
            return weatherForecast
        }
        return nil
    }
    
    func getWeatherCallbackResult(date: Date, callback: (Result<WeatherForecast, WeatherError>) -> ()) {
        precondition(url != nil)
        if chance(ints: 3...10) {
            let weatherForecast = WeatherForecast(date: date, temperature: 20)
            callback(Result.success(weatherForecast))
        } else {
            callback(Result.failure(WeatherError.error(message: "Bad chance")))
        }
    }
    
    func getWeatherCallbackOptionals(date: Date, callback: (WeatherForecast?, WeatherError?) -> ()) {
        precondition(url != nil)
        if chance(ints: 3...10) {
            let weatherForecast = WeatherForecast(date: date, temperature: 20)
            callback(weatherForecast, nil)
        } else {
            callback(nil, WeatherError.error(message: "Bad chance"))
        }
    }
    
}

let dataProvider = WeatherDataProvider.shared
dataProvider.configure(weatherServiceURL: URL(string: "pass")!)

do {
    try dataProvider.getWeatherThrows(date: Date())
} catch {
    print(error)
}

let weatherForecast1 = try? dataProvider.getWeatherOptional(date: Date())

let weatherForecast2 = try! dataProvider.getWeatherOptional(date: Date())
