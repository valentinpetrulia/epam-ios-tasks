import Foundation

enum error: Error {
    case negativeDiscriminant(Double)
}

func findRoots(a: Double, b: Double, c: Double) throws -> (x1: Double, x2: Double) {
    let D = b*b - 4*a*c
    if D >= 0 {
        let x1 = (-b + sqrt(D)) / (2*a)
        let x2 = (-b + sqrt(D)) / (2*a)
        return (x1, x2)
    } else {
        throw error.negativeDiscriminant(D)
    }
}

do {
    let roots = try findRoots(a: 1, b: 4, c: 16)
    print(roots)
} catch error.negativeDiscriminant(let D) {
    print("D = \(D). Roots are complex.")
}

