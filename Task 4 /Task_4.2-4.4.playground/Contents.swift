import Foundation

struct ComplexNumber {
    let a: Double
    let b: Double
}

func +(num1: Double, num2: ComplexNumber) -> ComplexNumber {
    return ComplexNumber(a: num1 + num2.a, b: num2.b)
}

func -(num1: Double, num2: ComplexNumber) -> ComplexNumber {
    return ComplexNumber(a: num1 - num2.a, b: -num2.b)
}

func /(num1: ComplexNumber, num2: Double) -> ComplexNumber {
    return ComplexNumber(a: num1.a / num2, b: num1.b / num2)
}

func complexSqrt(_ num: Double) -> ComplexNumber {
    return ComplexNumber(a: 0, b: sqrt(abs(num)))
}

func findRoots(a: Double, b: Double, c: Double) -> (x1: ComplexNumber, x2: ComplexNumber) {
    let D = b*b - 4*a*c
    if D >= 0 {
        let x1 = (-b + sqrt(D)) / (2*a)
        let x2 = (-b - sqrt(D)) / (2*a)
        return (x1: ComplexNumber(a: x1, b: 0), x2: ComplexNumber(a: x2, b: 0))
    } else {
        let x1 = (-b + complexSqrt(D)) / (2*a)
        let x2 = (-b - complexSqrt(D)) / (2*a)
        return (x1, x2)
    }
}

print(findRoots(a: 1, b: 7, c: 10))
print(findRoots(a: 1, b: 0, c: 49))
print(findRoots(a: 3, b: 1, c: 16))
