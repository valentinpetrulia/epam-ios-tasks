import UIKit

func multiplyMatrices(a: [[Double]], b: [[Double]]) -> [[Double]] {
    let start = DispatchTime.now()
    let m = a.count
    let n = b[0].count
    let l = b.count
    var c = Array(repeating: Array(repeating: 0.0, count: n), count: m)
    for i in 0..<m {
        for j in 0..<n {
            c[i][j] = 0
            for k in 0..<l {
                c[i][j] += a[i][k]*b[k][j]
            }
        }
    }
    let end = DispatchTime.now()
    let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
    let timeInterval = Double(nanoTime) / 1_000_000_000
    print("Time Interval 1: \(timeInterval)")
    return c
}

func multiplyMatricesSerialQueue(a: [[Double]], b: [[Double]]) -> [[Double]] {
    let start = DispatchTime.now()
    let m = a.count
    let n = b[0].count
    let l = b.count
    var c = Array(repeating: Array(repeating: 0.0, count: n), count: m)
    let mySerialQueue = DispatchQueue(label: "serialQueue")
    for i in 0..<m {
        for j in 0..<n {
            mySerialQueue.async {
                c[i][j] = 0
                for k in 0..<l {
                    c[i][j] += a[i][k]*b[k][j]
                }
            }
        }
    }
    let end = DispatchTime.now()
    let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
    let timeInterval = Double(nanoTime) / 1_000_000_000
    print("Time Interval 2: \(timeInterval)")
    return c
}


func multiplyMatricesConcurrentQueue(a: [[Double]], b: [[Double]]) -> [[Double]] {
    let start = DispatchTime.now()
    let m = a.count
    let n = b[0].count
    let l = b.count
    var c = Array(repeating: Array(repeating: 0.0, count: n), count: m)
    let myConcurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: .concurrent)
    for i in 0..<m {
        for j in 0..<n {
            myConcurrentQueue.async {
                c[i][j] = 0
                for k in 0..<l {
                    c[i][j] += a[i][k]*b[k][j]
                }
            }
        }
    }
    let end = DispatchTime.now()
    let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
    let timeInterval = Double(nanoTime) / 1_000_000_000
    print("Time Interval 3: \(timeInterval)")
    return c
}


let a: [[Double]] = [[0, 2], [3, 4], [5, 6]]
let b: [[Double]] = [[1, 3], [2,1]]
let c1 = multiplyMatrices(a: a, b: b)
let c2 = multiplyMatricesSerialQueue(a: a, b: b)
let c3 = multiplyMatricesConcurrentQueue(a: a, b: b)

print(c1)
print(c2)
print(c3)

