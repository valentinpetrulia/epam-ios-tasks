import Foundation

struct Employee {
    var firstName: String
    var lastName: String
}


struct Product {
    var name: String
}


class Company {
    var list: [Employee]
    var name: String
    
    required init() {
        self.list = [Employee]()
        self.name = ""
    }
    
    init(list: [Employee], name: String) {
        self.list = list
        self.name = name
    }
    
    convenience init?(employee: Employee?, name: String) {
        if let emp = employee {
            self.init(list: [emp], name: name)
        } else {
            return nil
        }
    }
    
    func getProduct(productName: String) -> Product {
        return Product(name: productName)
    }
    
}


class StateRegistry {
    static var registeredCompanies = [Company]()
    
    static func addCompany(_ company: Company) {
        StateRegistry.registeredCompanies.append(company)
    }
    
    static func registeredCompany(name: String) -> Bool {
        return StateRegistry.registeredCompanies.contains { $0.name == name }
    }
}


class FoodCompany: Company {
    var qualityCertificate: String
    
    required init() {
        self.qualityCertificate = ""
        super.init()
    }
    
    init?(employee: (String?, String?), name: String, qualityCerificate: String) {
        guard let empName = employee.0, let empSurname = employee.1 else { return nil }
        if empName.isEmpty || empSurname.isEmpty || name.isEmpty || qualityCerificate.isEmpty {
            return nil
        } else {
            self.qualityCertificate = qualityCerificate
            super.init(list: [Employee(firstName: empName, lastName: empSurname)], name: name)
        }
    }
}


class Project {
    var contractor: Company
    var name: String
    
    init?(name: String, company: Company) {
        if StateRegistry.registeredCompany(name: company.name) {
            self.contractor = company
            self.name = name
        } else {
            return nil
        }
    }
}

let company1 = Company(employee: Employee(firstName: "Sarah", lastName: "Parker"), name: "Parker Inc.")
let company2 = Company(list: [Employee(firstName: "John", lastName: "McTavish"),
                                                  Employee(firstName: "John", lastName: "Price"),
                                                  Employee(firstName: "Simon", lastName: "Riley")],
                                                  name: "Task 141")
let company3 = FoodCompany(employee: ("Frank", "Woods"), name: "Black Ops", qualityCerificate: "dskjdnaksd")

StateRegistry.addCompany(company1!)
StateRegistry.addCompany(company2)
StateRegistry.addCompany(company3!)

let project1 = Project(name: "secret project1", company: company1!)
let project2 = Project(name: "secret project1", company: company2)
