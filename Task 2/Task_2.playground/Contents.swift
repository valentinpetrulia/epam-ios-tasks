import Foundation

let allStudents: Set<Int> = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let monday: Set<Int> = [1, 2, 5, 6, 7]
let tuesday: Set<Int> = [3, 6, 8, 10]
let wednesday: Set<Int> = [1, 3, 7, 9, 10]

let threeDays = monday.intersection(tuesday).intersection(wednesday)
print(threeDays)

let twoDays = monday.intersection(tuesday).union(monday.intersection(wednesday)).union(tuesday.intersection(wednesday))
print(twoDays)

let thirdTask = (monday.intersection(wednesday)).subtracting(tuesday)
print(thirdTask)

let missedAllClasses = allStudents.subtracting(monday.union(tuesday).union(wednesday))
print(missedAllClasses)
