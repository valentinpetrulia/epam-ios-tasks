import UIKit

enum PersonAction {
    case walk
    case sleep
    case work
}

typealias PersonCallback = ((PersonAction) -> Void)

class Person {
    weak var workPlace: Factory?
    let name: String
    let surname: String
    private var callback: PersonCallback?
    
    init(name: String, surname: String) {
        self.name = name
        self.surname = surname
        
        callback = { [unowned self] action in
            switch action {
            case .walk:
                self.somebodyWalk()
            case .sleep:
                self.somebodySleep()
            case .work:
                self.somebodyWork()
            }
        }
    }
    
    deinit {
        print("\(name) \(surname) is deinitialized")
    }
    
    func walk() {
        callback?(.walk)
    }
    
    func sleep() {
        UIView.animate(withDuration: 1.0) { [weak self] in
            self?.callback?(.sleep)
        }
    }
    
    func work() {
        callback?(.work)
    }
    
    private func somebodyWalk() {
        print("\(name) \(surname) is walking")
    }
    
    private func somebodySleep() {
        print("\(name) \(surname) is sleeping")
    }
    
    private func somebodyWork() {
        print("\(name) \(surname) is working")
    }
}

class PersonCar {
    weak var owner: Person?
    let type: String
    
    init(owner: Person?, type: String) {
        self.owner = owner
        self.type = type
    }
}

class Factory {
    weak var worker: Person?
    var workers: [Person?] = []
    var callback: (() -> Void)?
    let factoryName: String
    
    init(factoryName: String) {
        self.factoryName = factoryName
        callback = { [weak self] in
            guard let self = self else { return }
            self.makeSomeFire {
                self.printFarewell()
            }
        }
    }
    
    deinit {
        print("Factory is deinitialized")
    }
    
    func addWorker(_ worker: Person) {
        self.worker = worker
        workers.append(worker)
        print("Now in factory are \(workers.count) employees. They are \(workers.map { $0!.name })")
    }
    
    func fireAllEmployees() {
        callback?()
    }
    
    private func makeSomeFire(callback: (() -> Void)?) {
        callback?()
        workers = []
    }
    
    private func printFarewell() {
        print("Say goodbye to our employees: \(workers.compactMap { $0!.name })")
    }
}

var zavod: Factory?
var john: Person?

zavod = Factory(factoryName: "Zavod")
john = Person(name: "John", surname: "Doe")
let jack = Person(name: "Jack", surname: "Appleseed")
let johnsCar = PersonCar(owner: john, type: "Sedan")

zavod!.addWorker(john!)
zavod!.addWorker(jack)
zavod!.fireAllEmployees()
print(zavod!.workers)

john!.workPlace = zavod
john!.walk()
john!.sleep()
john!.work()

zavod = nil
john = nil



