import Foundation

indirect enum LinkedListNode {
    case node(value: String, next: LinkedListNode)
    case endNode(value: String)
    
    func getValue() -> String {
        switch self {
        case .node(value: let value, next: _):
            return value
        case .endNode(value: let value):
            return value
        }
    }
}

extension LinkedListNode: CustomStringConvertible {
    var description: String {
        self.getValue()
    }
}

extension Array where Element == LinkedListNode {
    mutating func append(value: String) {
        let newNode = LinkedListNode.endNode(value: value)
        if let lastNode = self.last {
            let lastNodeValue = lastNode.getValue()
            self[count - 1] = LinkedListNode.node(value: lastNodeValue, next: newNode)
        }
        self.append(newNode)
    }
}


let third = LinkedListNode.endNode(value: "3")
let second = LinkedListNode.node(value: "2", next: third)
let first = LinkedListNode.node(value: "1", next: second)
var array = [first, second, third]
array.append(value: "test1")
array.append(value: "test2")
print(array)
