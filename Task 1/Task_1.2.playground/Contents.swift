import Foundation

func findRoots(a: Double, b: Double, c: Double) {
    let D = pow(b, 2) - 4 * a * c
    if D < 0 {
        print("Roots are complex")
        return
    }
    let root1 = (-b + sqrt(D)) / (2 * a)
    let root2 = (-b - sqrt(D)) / (2 * a)
    print("\(a)*x^2 + \(b)*x + \(c) = 0, root1 = \(root1), root2 = \(root2)")
}

findRoots(a: 1, b: -4, c: 4)
