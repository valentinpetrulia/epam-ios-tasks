import Foundation

func arithmeticMean(_ nums: Int...) -> Double {
    return Double(nums.reduce(0, +)) / Double(nums.count)
}

func geometricMean(_ nums: Int...) -> Double {
    return pow(Double((nums.reduce(1, *))), 1/Double(nums.count))
}

var a: Int = 2
var b: Int = 5
var c: Int = 4
var d: Int = 1
var e: Int = 2

print(arithmeticMean(a, b, c, d, e))
print(geometricMean(a, b, c, d, e))
