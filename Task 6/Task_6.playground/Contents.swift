import Foundation

let ex = [1, 2, 12, 5, 7, 88]

print(ex.filter{$0 % 2 == 0})
print(ex.reduce(0, +))

let exString = ["1", "2", "12", "5", "7", "88"]
print(exString.compactMap{Int($0)})
