import Foundation

class Node {
    var data: Int
    var left: Node?
    var right: Node?
    
    init(data: Int) {
        self.data = data
    }
}

func createBinaryTree(array: [Int], startIndex: Int, endIndex: Int) -> Node? {
    if startIndex > endIndex { return nil }
    let mid = (startIndex + endIndex) / 2
    let node = Node(data: array[mid])
    node.left = createBinaryTree(array: array, startIndex: startIndex, endIndex: mid-1)
    node.right = createBinaryTree(array: array, startIndex: mid+1, endIndex: endIndex)
    return node
}

func printInOrder(_ node: Node?) {
    if let node = node {
        printInOrder(node.left)
        print(node.data)
        printInOrder(node.right)
    }
}

let sortedArray = [-1, -10, 5, 2, 8, 4, 91, -54].sorted()
let binaryTree = createBinaryTree(array: sortedArray, startIndex: 0, endIndex: sortedArray.count-1)
printInOrder(binaryTree)
